//
//  TestClass.swift
//  TestDynamicFramework
//
//  Created by Piotr Giza on 21.09.2017.
//  Copyright © 2017 Piotr Giza. All rights reserved.
//

import Foundation
import VideoKit

public class SwiftFramework {
    private var vkPlayerController: VKManager?
    
    init () {
        print("✅ Class has been initialised")
        
    }
    
    func doSomething() {
        print("✅ Yeah, it works")
    }
}
