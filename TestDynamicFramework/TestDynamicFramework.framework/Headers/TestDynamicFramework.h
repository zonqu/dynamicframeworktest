//
//  TestDynamicFramework.h
//  TestDynamicFramework
//
//  Created by Piotr Giza on 21.09.2017.
//  Copyright © 2017 Piotr Giza. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TestDynamicFramework.
FOUNDATION_EXPORT double TestDynamicFrameworkVersionNumber;

//! Project version string for TestDynamicFramework.
FOUNDATION_EXPORT const unsigned char TestDynamicFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TestDynamicFramework/PublicHeader.h>


